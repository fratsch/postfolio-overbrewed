# Postfolio - Overbrewed

This game jam project was created as part of the Game Dev Week in Trier.

The team consisted of three programmers and two designers.

Inspired by games like Overcooked, Overbrewed's core gameplay was brewing different variations of beer depending on customer orders.
The ingredients have to be assembled in a pot, brewed in the time machine, then filled into glasses and possibly topped with sirup.
The beer is then delivered via beerzooka (of course).

My main tasks included the recipe and brewing system as well as inventory and item placement.

Only files corresponding to these tasks are included.

Files which are needed for context but have not been written by me are marked by a comment in line one.