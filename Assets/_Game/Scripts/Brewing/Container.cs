﻿using System.Collections.Generic;
using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	public class Container : MonoBehaviour
	{
		private ContainerType currentContainer;

		private readonly List<LiquidIngredientType> currentLiquids = new List<LiquidIngredientType>();
		private readonly List<IngredientType> currentIngredients = new List<IngredientType>();

		public void ResetContainer()
		{
			currentIngredients.Clear();
			currentLiquids.Clear();
			currentContainer = null;
		}

		public void ClearContents()
		{
			currentIngredients.Clear();
			currentLiquids.Clear();
		}

		public bool AddContainer(ContainerType container)
		{
			if (currentContainer != null) return false;

			currentContainer = container;

			return true;
		}

		public bool AddIngredient(IngredientType ingredient)
		{
			if (currentContainer == null) return false;

			if (ingredient is LiquidIngredientType type)
			{
				if (currentLiquids.Contains(type)) return false;

				currentLiquids.Add(type);

				return true;
			}
			else if (!currentIngredients.Contains(ingredient))
			{
				currentIngredients.Add(ingredient);

				return true;
			}

			return false;
		}

		public ContainerType GetContainer()
		{
			return currentContainer;
		}

		public List<IngredientType> GetIngredients()
		{
			return new List<IngredientType>(currentIngredients);
		}

		public List<LiquidIngredientType> GetLiquids()
		{
			return new List<LiquidIngredientType>(currentLiquids);
		}
	}
}
