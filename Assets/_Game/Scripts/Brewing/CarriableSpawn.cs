﻿using System.Collections;
using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	public class CarriableSpawn : MonoBehaviour
	{
		[SerializeField] private Carriable carriable;

		[SerializeField] private float dispenseTime;

		public bool fluidSpawn;

		private bool isLocked;

		private GameObject streamEffect;

		private ProgressBar progressBar;

		private AudioSource audioSource;

		private void Start()
		{
			progressBar = GetComponentInChildren<ProgressBar>();

			audioSource = GetComponent<AudioSource>();

			if (progressBar != null)
			{
				progressBar.gameObject.SetActive(false);
			}

			foreach (Transform t in transform)
			{
				if (t.tag.Equals("StreamEffect"))
				{
					streamEffect = t.gameObject;
				}
			}
		}

		public Carriable TakeCarriable(PlayerActor playerActor, Carriable car)
		{
			if (isLocked) return null;

			StartCoroutine(LockPlayerControl(dispenseTime, playerActor, car));

			return carriable;
		}

		private IEnumerator LockPlayerControl(float duration, PlayerActor playerActor, Carriable car)
		{
			isLocked = true;
			playerActor.PlayerMovement.CanMove = false;
			playerActor.PlayerStateManager.stopWalking();

			if (streamEffect != null)
			{
				streamEffect.SetActive(true);
			}

			if (progressBar != null)
			{
				progressBar.InitializeBar(duration);
			}

			if (audioSource != null)
			{
				audioSource.Play();
			}

			yield return new WaitForSeconds(duration);

			if (car is ContainerType)
			{
				((ContainerType) car).isEmpty = false;
				playerActor.CarryItem.UpdateGameObject();
			}

			if (streamEffect != null)
			{
				streamEffect.SetActive(false);
			}

			isLocked = false;
			playerActor.PlayerMovement.CanMove = true;
		}
	}
}
