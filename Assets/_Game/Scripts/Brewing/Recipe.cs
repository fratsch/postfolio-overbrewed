﻿using System.Collections.Generic;
using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	[CreateAssetMenu(fileName = "New Recipe", menuName = "OverBrewed/Brewing/Recipe", order = 2)]
	public class Recipe : ScriptableObject
	{
		[Tooltip("The required ingredients for this recipe.")] [SerializeField]
		private List<IngredientType> requiredIngredients;

		[Tooltip("The required container for this recipe.")] [SerializeField]
		private ContainerType requiredContainer;

		[Tooltip("The produced ingredient.")] [SerializeField]
		private IngredientType producedIngredient;

		[Tooltip("The time this cooking process takes.")] [SerializeField]
		private float cookingTime;

		// returns a list of the required ingredients for this recipe
		public List<IngredientType> GetIngredients()
		{
			return new List<IngredientType>(requiredIngredients);
		}

		// returns the type of the required utensil
		public ContainerType GetContainer()
		{
			return requiredContainer;
		}

		// returns the type of the produced ingredient
		public IngredientType GetProduct()
		{
			return producedIngredient;
		}

		//returns the time this recipe takes to finish
		public float GetCookingTime()
		{
			return cookingTime;
		}

		// returns true if the recipe contains the ingredient
		public bool CheckForIngredient(IngredientType ingredient)
		{
			return requiredIngredients.Contains(ingredient);
		}

		// returns true if the recipe contains the utensil
		public bool CheckForContainer(ContainerType container)
		{
			return container == requiredContainer;
		}
	}
}
