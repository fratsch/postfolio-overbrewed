﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	public class Tap : MonoBehaviour
	{
		[SerializeField] private List<Recipe> recipes = new List<Recipe>();

		[SerializeField] private IngredientType beerGlas;

		[SerializeField] private int servings = 1;

		[SerializeField] private float fillTime = 1.0f;

		[SerializeField] private float minTapTime = 1.0f;

		[SerializeField] private float maxTapTime = 3.0f;

		[SerializeField] private AudioClip fillingClip;

		[SerializeField] private AudioClip tappingClip;

		private LiquidIngredientType liquid;

		private IngredientType glass;

		private IngredientType beverage;

		private GameObject itemSpawn;

		private GameObject streamEffect;

		private ProgressBar progressBar;

		private AudioSource audioSource;
		private int servingsLeft = 0;

		private bool isLocked;

		private void Start()
		{
			progressBar = GetComponentInChildren<ProgressBar>();

			audioSource = GetComponent<AudioSource>();

			if (progressBar != null)
			{
				progressBar.gameObject.SetActive(false);
			}

			foreach (Transform t in transform)
			{
				if (t.tag.Equals("ItemSpawn"))
				{
					itemSpawn = t.gameObject;
				}
			}

			foreach (Transform t in transform)
			{
				if (t.tag.Equals("StreamEffect"))
				{
					streamEffect = t.gameObject;
				}
			}
		}

		public bool AddLiquid(LiquidIngredientType liquid, PlayerActor playerActor)
		{
			if (this.liquid != null) return false;

			bool hit = false;

			foreach (Recipe recipe in recipes)
			{
				if (recipe.CheckForIngredient(liquid))
				{
					hit = true;
				}
			}

			if (!hit) return false;

			this.liquid = liquid;

			StartCoroutine(FillLiquid(playerActor));

			return true;
		}

		public bool AddGlass(Carriable glass)
		{
			if (this.glass != null || glass != beerGlas || liquid == null) return false;

			bool hit = false;

			foreach (Recipe recipe in recipes)
			{
				if (!recipe.CheckForIngredient((IngredientType) glass)) continue;

				hit = true;
				this.glass = (IngredientType) glass;
			}

			UpdateGameObjects();

			return hit;
		}

		public IngredientType RemoveBeverage()
		{
			if (servingsLeft-- <= 0) return null;

			IngredientType beverage = this.beverage;
			this.beverage = null;

			UpdateGameObjects();

			return beverage;
		}

		public bool IsLocked()
		{
			return isLocked;
		}

		public bool StartTapping(PlayerActor playerActor)
		{
			Recipe currentRecipe = null;

			foreach (Recipe recipe in recipes)
			{
				if (recipe.CheckForIngredient(liquid) && recipe.CheckForIngredient(glass))
				{
					currentRecipe = recipe;
				}
			}

			if (currentRecipe == null)
			{
				return false;
			}

			isLocked = true;

			const float SUCCESS = 1.0f;

			StartCoroutine(TapBeverage(Mathf.Lerp(minTapTime, maxTapTime, SUCCESS), currentRecipe, playerActor));

			return true;
		}

		private IEnumerator FillLiquid(PlayerActor playerActor)
		{
			playerActor.PlayerMovement.CanMove = false;
			playerActor.PlayerStateManager.stopWalking();

			progressBar.InitializeBar(fillTime);

			audioSource.clip = fillingClip;
			audioSource.Play();

			yield return new WaitForSeconds(fillTime);

			playerActor.PlayerMovement.CanMove = true;
		}

		private IEnumerator TapBeverage(float duration, Recipe recipe, PlayerActor playerActor)
		{
			playerActor.PlayerMovement.CanMove = false;
			playerActor.PlayerStateManager.stopWalking();

			if (streamEffect != null)
			{
				streamEffect.SetActive(true);
			}

			if (progressBar != null)
			{
				progressBar.InitializeBar(duration);
			}

			audioSource.clip = tappingClip;
			audioSource.Play();

			yield return new WaitForSeconds(duration);

			if (streamEffect != null)
			{
				streamEffect.SetActive(false);
			}

			playerActor.PlayerMovement.CanMove = true;

			liquid = null;

			glass = null;

			servingsLeft = servings;

			beverage = recipe.GetProduct();

			UpdateGameObjects();

			isLocked = false;
		}

		// spawns GameObject for the Carriable at CarriableSpot
		private void UpdateGameObjects()
		{
			foreach (Transform t in itemSpawn.transform)
			{
				Destroy(t.gameObject);
			}

			if (glass != null)
			{
				GameObject newObject = Instantiate(glass.GetPrefab(), itemSpawn.transform.position,
					itemSpawn.transform.rotation);
				newObject.transform.parent = itemSpawn.transform;
			}
			else if (beverage != null)
			{
				GameObject newObject = Instantiate(beverage.GetPrefab(), itemSpawn.transform.position,
					itemSpawn.transform.rotation);
				newObject.transform.parent = itemSpawn.transform;
			}
		}
	}
}
