﻿using System.Collections.Generic;
using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	public class Customer : MonoBehaviour
	{
		[SerializeField] private Order customerOrder;

		private List<IngredientType> requiredIngredients;

		private void Start()
		{
			requiredIngredients = customerOrder.GetIngredients();
		}

		public bool GiveIngredient(IngredientType ingredient)
		{
			if (!requiredIngredients.Contains(ingredient)) return false;

			requiredIngredients.Remove(ingredient);

			return true;
		}
	}
}
