﻿using UnityEngine;

namespace Scripts.Brewing.ItemTypes
{
	[CreateAssetMenu(fileName = "New IngredientType", menuName = "OverBrewed/Brewing/IngredientType", order = 3)]
	public class IngredientType : Carriable
	{

	}
}
