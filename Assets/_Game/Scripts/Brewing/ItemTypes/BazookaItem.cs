﻿using UnityEngine;

namespace Scripts.Brewing.ItemTypes
{
	[CreateAssetMenu(fileName = "New Bazooka", menuName = "OverBrewed/Brewing/Bazooka")]
	public class BazookaItem : Carriable
	{

	}
}
