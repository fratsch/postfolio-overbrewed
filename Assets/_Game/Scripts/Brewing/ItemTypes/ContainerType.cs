﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Brewing.ItemTypes
{
	[CreateAssetMenu(fileName = "New ContainerType", menuName = "OverBrewed/Brewing/ContainerType", order = 6)]
	public class ContainerType : Carriable
	{
		[SerializeField] private List<IngredientType> allowedIntregients;

		[SerializeField] private GameObject closedPrefab;

		[SerializeField] private GameObject emptyPrefab;

		public bool isOpen = true;

		public bool isEmpty = true;

		public new GameObject GetPrefab()
		{
			return isOpen ? isEmpty ? emptyPrefab : prefab : closedPrefab;
		}

		public bool CheckAllowedIngredients(IngredientType ingredient)
		{
			return allowedIntregients.Contains(ingredient);
		}
	}
}
