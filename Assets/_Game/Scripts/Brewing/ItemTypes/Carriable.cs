﻿using System;
using UnityEngine;

namespace Scripts.Brewing.ItemTypes
{
	[Serializable]
	public class Carriable : ScriptableObject
	{
		[Tooltip("The prefab of the item that should be instantiated.")]
		[SerializeField] protected GameObject prefab;

		[SerializeField] protected AudioClip audio;

		public GameObject GetPrefab()
		{
			return prefab;
		}

		public AudioClip GetAudioClip()
		{
			return audio;
		}
	}
}
