﻿using UnityEngine;

namespace Scripts.Brewing.ItemTypes
{
	[CreateAssetMenu(fileName = "New LiquidIngredientType", menuName = "OverBrewed/Brewing/LiquidIngredientType", order = 4)]
	public class LiquidIngredientType : IngredientType
	{

	}
}
