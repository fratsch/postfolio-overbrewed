﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scripts.Brewing.ItemTypes;
using Scripts.VFX;
using UnityEngine;

namespace Scripts.Brewing
{
	public class Worktop : MonoBehaviour
	{
		[SerializeField] private List<Recipe> recipes;

		public bool needsContainer;

		private List<IngredientType> _currentIngredients = new List<IngredientType>();
		private List<LiquidIngredientType> _currentLiquids = new List<LiquidIngredientType>();

		private List<Recipe> _possibleRecipes = new List<Recipe>();

		private bool inputBlocked;

		private bool isLocked;

		private ContainerType currentContainer;

		private GameObject itemSpawn;

		private ProgressBar progressBar;

		private SlidingDoor slidingDoor;

		private AudioSource audioSource;

		private void Start()
		{
			_possibleRecipes.AddRange(recipes);

			audioSource = GetComponent<AudioSource>();

			foreach (Transform t in transform)
			{
				if (t.tag.Equals("ItemSpawn"))
				{
					itemSpawn = t.gameObject;
				}
			}

			progressBar = GetComponentInChildren<ProgressBar>();

			if (progressBar != null)
			{
				progressBar.gameObject.SetActive(false);
			}

			slidingDoor = GetComponentInChildren<SlidingDoor>();
		}

		// resets the private fields
		public void ResetWorktop()
		{
			_possibleRecipes.Clear();
			_possibleRecipes.AddRange(recipes);

			_currentIngredients.Clear();
			_currentLiquids.Clear();
			currentContainer = null;

			inputBlocked = false;
			isLocked = false;

			UpdateGameObjects();
		}

		public bool PutDownCarriable(Carriable carriable)
		{
			if (inputBlocked) return false;

			switch (carriable)
			{
				case IngredientType ingredient:
					return AddIngredient(ingredient);
				case ContainerType container:
					return AddContainer(container);
				default:
					return false;
			}
		}

		// checks if there is space for the ingredient and if it is needed
		// returns whether there was space for the ingredient
		private bool AddIngredient(IngredientType ingredient)
		{
			bool hit = false;

			if (!needsContainer && !_currentIngredients.Contains(ingredient))
			{
				foreach (Recipe recipe in _possibleRecipes.ToList())
				{
					if (!recipe.CheckForIngredient(ingredient))
					{
						_possibleRecipes.Remove(recipe);
					}
					else
					{
						hit = true;
					}
				}

				if (!hit)
				{
					ResetWorktop();
					return false;
				};

				_currentIngredients.Add(ingredient);

				UpdateGameObjects();

				return true;

			}
			else if (needsContainer && currentContainer != null)
			{
				if (ingredient is LiquidIngredientType && !_currentLiquids.Contains((LiquidIngredientType) ingredient))
				{
					foreach (Recipe recipe in _possibleRecipes.ToList())
					{
						if (!recipe.CheckForIngredient(ingredient))
						{
							_possibleRecipes.Remove(recipe);
						}
						else
						{
							hit = true;
						}
					}

					if (!hit) return false;

					_currentLiquids.Add((LiquidIngredientType) ingredient);

					return true;
				}
				else if (!_currentIngredients.Contains(ingredient))
				{
					foreach (Recipe recipe in _possibleRecipes.ToList())
					{
						if (!recipe.CheckForIngredient(ingredient))
						{
							_possibleRecipes.Remove(recipe);
						}
						else
						{
							hit = true;
						}
					}

					if (!hit) return false;

					_currentIngredients.Add(ingredient);

					return true;
				}
			}
			return false;
		}

		// checks if the Worktop needs a container and if it already has one
		// returns if the container could be placed
		private bool AddContainer(ContainerType container)
		{
			if (!needsContainer || currentContainer != null) return false;

			currentContainer = container;

			UpdateGameObjects();

			return true;
		}

		// returns the currently last ingredient and then removes it
		public IngredientType RemoveIngredient()
		{
			int count = _currentIngredients.Count;

			IngredientType ingredient = null;

			if (count <= 0) return null;

			ingredient = _currentIngredients[count - 1];
			_currentIngredients.Remove(ingredient);

			UpdateGameObjects();

			if (_currentIngredients.Count <= 0)
			{
				if (currentContainer == null)
				{
					ResetWorktop();
				}
				else
				{
					inputBlocked = false;
				}
			}

			return ingredient;
		}

		// returns all current ingredients and clears the list afterwards
		public List<IngredientType> RemoveIngredients()
		{
			List<IngredientType> ingredients = new List<IngredientType>(_currentIngredients);

			_currentIngredients.Clear();

			return ingredients;
		}

		// returns the currently last liquid and then removes it
		public LiquidIngredientType RemoveLiquid()
		{
			int count = _currentLiquids.Count;

			LiquidIngredientType ingredient = null;

			if (count <= 0) return null;

			ingredient = _currentLiquids[count - 1];
			_currentIngredients.Remove(ingredient);

			UpdateGameObjects();

			return ingredient;
		}

		// returns all current liquids and clears the list afterwards
		public List<LiquidIngredientType> RemoveLiquids()
		{
			List<LiquidIngredientType> liquids = new List<LiquidIngredientType>(_currentLiquids);

			_currentLiquids.Clear();

			return liquids;
		}

		// returns the current container and then sets it null
		public ContainerType RemoveContainer()
		{
			ContainerType container = currentContainer;
			currentContainer = null;

			inputBlocked = false;

			UpdateGameObjects();

			return container;
		}

		// returns the current container
		public ContainerType GetContainer()
		{
			return currentContainer;
		}

		public bool HasNoIngredients()
		{
			return _currentIngredients.Count <= 0;
		}

		public bool IsLocked()
		{
			return isLocked;
		}

		// spawns GameObject for the Carriable at CarriableSpot
		private void UpdateGameObjects()
		{
			foreach (Transform t in itemSpawn.transform)
			{
				Destroy(t.gameObject);
			}

			if (needsContainer && currentContainer != null)
			{
				GameObject newObject = Instantiate(currentContainer.GetPrefab(), itemSpawn.transform.position,
					itemSpawn.transform.rotation) as GameObject;
				newObject.transform.parent = itemSpawn.transform;
			}
			else if (!needsContainer && _currentIngredients.Count >= 1)
			{
				GameObject newObject = Instantiate(_currentIngredients[0].GetPrefab(), itemSpawn.transform.position,
					itemSpawn.transform.rotation) as GameObject;
				newObject.transform.parent = itemSpawn.transform;
			}
		}

		// checks if all ingredients for the recipe are present and if so, starts cooking
		// returns whether cooking was started
		public bool StartCooking()
		{
			if (_possibleRecipes.Count != 1 || _currentIngredients.Count <= 0) return false;

			foreach (IngredientType ingredient in _possibleRecipes[0].GetIngredients())
			{
				if (!_currentIngredients.Contains(ingredient) && !_currentLiquids.Contains(ingredient))
				{
					return false;
				}
			}

			isLocked = true;

			StartCoroutine(Cooking());

			return true;

		}

		// waits for cooking time seconds and then sets the product of the current recipe as ingredient one
		private IEnumerator Cooking()
		{
			inputBlocked = true;

			float duration = _possibleRecipes[0].GetCookingTime();

			if (slidingDoor != null)
			{
				slidingDoor.Slide(duration);
				duration += 2.0f;
			}

			if (progressBar != null)
			{
				progressBar.InitializeBar(duration);
			}

			audioSource.Play();

			yield return new WaitForSeconds(duration);

			IngredientType product = _possibleRecipes[0].GetProduct();
			ContainerType container = currentContainer;

			if (currentContainer != null)
			{
				currentContainer.isOpen = false;
			}

			ResetWorktop();

			if(!AddIngredient(product))
			{
				ResetWorktop();
				_currentIngredients.Add(product);
				inputBlocked = true;
			}

			AddContainer(container);

			UpdateGameObjects();

			isLocked = false;
		}
	}
}
