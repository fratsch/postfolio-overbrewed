﻿using System.Collections.Generic;
using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	[CreateAssetMenu(fileName = "New Order", menuName = "OverBrewed/Brewing/Order", order = 1)]
	public class Order : ScriptableObject
	{
		[Tooltip("The name of this order. May be used in UI representation!")] [SerializeField]
		private string orderName;

		[Tooltip("The ingredients that are required to fulfill this order!")] [SerializeField]
		private List<IngredientType> ingredients = new List<IngredientType>();

		[Tooltip("The Popup that will show up as the customer orders.")] [SerializeField]
		private GameObject orderPopup;

		public GameObject OrderPopup => orderPopup;

		// returns the name of this order
		public string GetName()
		{
			return orderName;
		}

		// return a list of the required ingredients for this order
		public List<IngredientType> GetIngredients()
		{
			return new List<IngredientType>(ingredients);
		}
	}
}
