﻿using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Brewing
{
	public class CarriableSpot : MonoBehaviour
	{
		[SerializeField] private Carriable spawnOnStart;

		private Carriable currentCarriable;

		private Container container;

		private GameObject itemSpawn;

		private void Start()
		{
			this.container = GetComponent<Container>();

			foreach (Transform t in transform)
			{
				if (t.tag.Equals("ItemSpawn"))
				{
					itemSpawn = t.gameObject;
				}
			}

			if (spawnOnStart == null) return;

			Container itemContainer = GetComponent<Container>();

			PutDownCarriable(spawnOnStart);

			if (!(spawnOnStart is ContainerType)) return;

			itemContainer.AddContainer((ContainerType) spawnOnStart);
			((ContainerType) spawnOnStart).isOpen = true;
			((ContainerType) spawnOnStart).isEmpty = true;

			UpdateGameObjects();
		}

		// checks if spot is already occupied, and puts down carriable if not
		// returns if spot was occupied
		public bool PutDownCarriable(Carriable carriable)
		{
			if (currentCarriable == null)
			{
				currentCarriable = carriable;

				UpdateGameObjects();

				return true;
			}

			if (!(currentCarriable is ContainerType) || !(carriable is IngredientType) ||
				!((ContainerType) currentCarriable).CheckAllowedIngredients((IngredientType) carriable)) return false;

			return container.AddIngredient((IngredientType) carriable);
		}

		// returns the currently present Carriable
		public Carriable TakeCarriable()
		{
			Carriable carriable = currentCarriable;
			currentCarriable = null;

			UpdateGameObjects();

			return carriable;
		}

		public IngredientType TakeIngredient()
		{
			IngredientType ingredient = null;

			if (!(currentCarriable is IngredientType)) return null;

			ingredient = (IngredientType) currentCarriable;
			currentCarriable = null;

			UpdateGameObjects();

			return ingredient;
		}

		// spawns GameObject for the Carriable at CarriableSpot
		private void UpdateGameObjects()
		{
			foreach (Transform t in itemSpawn.transform)
			{
				Destroy(t.gameObject);
			}

			if (currentCarriable == null) return;

			if (currentCarriable is ContainerType)
			{
				GameObject newObject = Instantiate(((ContainerType) currentCarriable).GetPrefab(),
					itemSpawn.transform.position,
					itemSpawn.transform.rotation);
				newObject.transform.parent = itemSpawn.transform;
			}
			else
			{
				GameObject newObject = Instantiate(currentCarriable.GetPrefab(), itemSpawn.transform.position,
					itemSpawn.transform.rotation);
				newObject.transform.parent = itemSpawn.transform;
			}
		}
	}
}
