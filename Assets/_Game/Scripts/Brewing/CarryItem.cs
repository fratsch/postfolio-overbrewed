﻿using Scripts.Brewing.ItemTypes;
using UnityEngine;
using UnityEngine.Video;

namespace Scripts.Brewing
{
	public class CarryItem : MonoBehaviour
	{
		[SerializeField] private float rayCastDistance = 2.0f;

		[SerializeField] private float rayCastHeight = 1.0f;

		[SerializeField] private AudioClip errorClip;

		private PlayerActor playerActor;

		private GameObject playerCarry;

		private IInputManager inputManager;

		private AudioSource audioSource;

		private AudioClip audioClip;

		private VideoPlayer videoPlayer;

		// the currently carried item
		private Carriable carriedItem;

		private void Start()
		{
			inputManager = GetComponent<IInputManager>();
			playerActor = GetComponent<PlayerActor>();
			audioSource = GetComponent<AudioSource>();
			if (Camera.main != null) videoPlayer = Camera.main.GetComponent<VideoPlayer>();

			foreach (Transform t in transform)
			{
				if (t.tag.Equals("PlayerCarry"))
				{
					playerCarry = t.gameObject;
				}
			}
		}

		private void PlayAudioClip()
		{
			if (audioClip == null) return;

			audioSource.clip = audioClip;
			audioSource.Play();
		}

		// spawns GameObject for the Carriable in player's hands
		public void UpdateGameObject()
		{
			foreach (Transform t in playerCarry.transform)
			{
				Destroy(t.gameObject);
			}

			if (carriedItem == null)
			{
				playerActor.PlayerStateManager.dropBazooka();
				playerActor.PlayerStateManager.dropItem();
			}
			else if (carriedItem is BazookaItem)
			{
				playerActor.PlayerStateManager.pickupBazooka();
			}
			else
			{
				playerActor.PlayerStateManager.pickupItem();

				if (carriedItem is ContainerType)
				{
					GameObject newObject = Instantiate(((ContainerType) carriedItem).GetPrefab(),
						playerCarry.transform.position,
						playerCarry.transform.rotation * Quaternion.AngleAxis(0.0f, Vector3.right));
					newObject.transform.parent = playerCarry.transform;
				}
				else
				{
					GameObject newObject = Instantiate(carriedItem.GetPrefab(), playerCarry.transform.position,
						playerCarry.transform.rotation * Quaternion.AngleAxis(0.0f, Vector3.right));
					newObject.transform.parent = playerCarry.transform;
				}
			}
		}

		private void PutItemInContainer(IngredientType liquid)
		{
			Container intoContainer = GetComponent<Container>();
			intoContainer.AddIngredient(liquid);
		}

		private void PutDownContainerOnSpot(GameObject other)
		{
			Container intoContainer = other.GetComponent<Container>();
			Container fromContainer = GetComponent<Container>();
			intoContainer.AddContainer(fromContainer.GetContainer());

			foreach (LiquidIngredientType ingredient in fromContainer.GetLiquids())
			{
				intoContainer.AddIngredient(ingredient);
			}

			foreach (IngredientType ingredient in fromContainer.GetIngredients())
			{
				intoContainer.AddIngredient(ingredient);
			}

			fromContainer.ResetContainer();
		}

		private void PickUpContainerFromSpot(GameObject other)
		{
			Container fromContainer = other.GetComponent<Container>();
			Container intoContainer = GetComponent<Container>();
			intoContainer.AddContainer(fromContainer.GetContainer());

			foreach (LiquidIngredientType ingredient in fromContainer.GetLiquids())
			{
				intoContainer.AddIngredient(ingredient);
			}

			foreach (IngredientType ingredient in fromContainer.GetIngredients())
			{
				intoContainer.AddIngredient(ingredient);
			}

			fromContainer.ResetContainer();
		}

		private bool PutDownContainerOnWorktop(GameObject other)
		{
			Worktop intoContainer = other.GetComponent<Worktop>();
			Container fromContainer = GetComponent<Container>();
			intoContainer.PutDownCarriable(fromContainer.GetContainer());

			if (intoContainer.IsLocked()) return false;

			bool error = false;

			foreach (LiquidIngredientType ingredient in fromContainer.GetLiquids())
			{
				if (!intoContainer.PutDownCarriable(ingredient))
				{
					error = true;
				}
			}

			foreach (IngredientType ingredient in fromContainer.GetIngredients())
			{
				if (!intoContainer.PutDownCarriable(ingredient))
				{
					error = true;
				}
			}

			if (error)
			{
				intoContainer.ResetWorktop();
			}
			else
			{
				fromContainer.ResetContainer();
			}

			return error;
		}

		private void PickUpContainerFromWorktop(GameObject other)
		{
			Worktop fromContainer = other.GetComponent<Worktop>();
			Container intoContainer = GetComponent<Container>();

			intoContainer.AddContainer(fromContainer.RemoveContainer());

			foreach (LiquidIngredientType ingredient in fromContainer.RemoveLiquids())
			{
				intoContainer.AddIngredient(ingredient);
			}

			foreach (IngredientType ingredient in fromContainer.RemoveIngredients())
			{
				intoContainer.AddIngredient(ingredient);
			}
		}

		private bool PourContainerIntoTap(GameObject other)
		{
			Container fromContainer = GetComponent<Container>();
			Tap tap = other.GetComponent<Tap>();

			if (fromContainer.GetIngredients().Count != 0 || fromContainer.GetLiquids().Count != 1) return false;

			if (!tap.AddLiquid(fromContainer.GetLiquids()[0], playerActor)) return false;

			fromContainer.ResetContainer();
			fromContainer.AddContainer((ContainerType) carriedItem);

			return true;
		}

		private void Update()
		{
			var transform1 = transform;
			var position = transform1.position;
			Debug.DrawRay(new Vector3(position.x, rayCastHeight, position.z),
				transform1.forward * rayCastDistance, Color.green, 0, false);
			if (!inputManager.Interact()) return;

			if (!Physics.Raycast(new Vector3(transform.position.x, rayCastHeight, transform.position.z),
				transform.forward,
				out RaycastHit hit, rayCastDistance, LayerMask.GetMask("Environment"))) return;

			GameObject other = hit.collider.gameObject;
			string otherTag = other.tag;

			if (carriedItem != null)
			{
				audioClip = carriedItem.GetAudioClip();

				if (otherTag.Equals("Dispenser"))
				{
					if (carriedItem is ContainerType &&
						other.GetComponent<CarriableSpawn>().fluidSpawn)
					{
						PutItemInContainer((IngredientType) other.GetComponent<CarriableSpawn>()
							.TakeCarriable(playerActor, carriedItem));
						audioClip = null;
					}
					else
					{
						audioClip = errorClip;
					}
				}
				else if (otherTag.Equals("Spot"))
				{
					bool loadedBazooka = false;
					if (carriedItem is BazookaItem)
					{
						IngredientType ingredient = other.GetComponent<CarriableSpot>().TakeIngredient();
						if (ingredient != null)
						{
							IngredientType ammunition = playerActor.Bazooka.LoadBazooka(ingredient);
							loadedBazooka = true;
							other.GetComponent<CarriableSpot>().PutDownCarriable(ammunition);
						}
					}

					if (!loadedBazooka && other.GetComponent<CarriableSpot>().PutDownCarriable(carriedItem))
					{
						if (carriedItem is ContainerType)
						{
							PutDownContainerOnSpot(other);
						}

						carriedItem = null;
					}
					else if (carriedItem == null)
					{
						audioClip = errorClip;
					}
				}
				else if (otherTag.Equals("Worktop") && !other.GetComponent<Worktop>().IsLocked())
				{
					switch (carriedItem)
					{
						case ContainerType _ when other.GetComponent<Worktop>().GetContainer() == null && other.GetComponent<Worktop>().HasNoIngredients() && other.GetComponent<Worktop>().needsContainer:
						{
							if (!PutDownContainerOnWorktop(other))
							{
								other.GetComponent<Worktop>().StartCooking();
								carriedItem = null;
							}
							else
							{
								audioClip = errorClip;
							}

							break;
						}
						case IngredientType type when other.GetComponent<Worktop>()
							.PutDownCarriable(type):
							other.GetComponent<Worktop>().StartCooking();
							carriedItem = null;
							break;
						default:
							audioClip = errorClip;
							break;
					}
				}
				else if (otherTag.Equals("Tap") && !other.GetComponent<Tap>().IsLocked())
				{
					if (carriedItem is ContainerType)
					{
						if (PourContainerIntoTap(other))
						{
							((ContainerType) carriedItem).isOpen = true;
							((ContainerType) carriedItem).isEmpty = true;
						}
						else
						{
							audioClip = errorClip;
						}
					}
					else
					{
						if (other.GetComponent<Tap>().AddGlass(carriedItem))
						{
							carriedItem = null;

							other.GetComponent<Tap>().StartTapping(GetComponent<PlayerActor>());
						}
						else
						{
							audioClip = errorClip;
						}
					}
				}
				else if (otherTag.Equals("Garbage"))
				{
					if (carriedItem is ContainerType)
					{
						GetComponent<Container>().ClearContents();
						((ContainerType) carriedItem).isOpen = true;
						((ContainerType) carriedItem).isEmpty = true;
					}
					else
					{
						if (carriedItem is BazookaItem)
						{
							videoPlayer.loopPointReached += (vp) => { vp.enabled = false; };
							videoPlayer.Play();
						}

						carriedItem = null;
					}
				}
			}
			else
			{
				if (otherTag.Equals("Dispenser"))
				{
					if (other.GetComponent<CarriableSpawn>().fluidSpawn) return;

					carriedItem = other.GetComponent<CarriableSpawn>()
						.TakeCarriable(playerActor, carriedItem);
				}
				else if (otherTag.Equals("Spot"))
				{
					carriedItem = other.GetComponent<CarriableSpot>().TakeCarriable();

					if (carriedItem is ContainerType)
					{
						PickUpContainerFromSpot(other);
					}
					else
					{
						audioClip = null;
					}
				}
				else if (otherTag.Equals("Worktop") && !other.GetComponent<Worktop>().IsLocked())
				{
					Worktop worktop = other.GetComponent<Worktop>();

					if (!worktop.IsLocked())
					{
						carriedItem = worktop.GetContainer();

						if (carriedItem != null)
						{
							PickUpContainerFromWorktop(other);
						}
						else
						{
							carriedItem = worktop.RemoveIngredient();
						}
					}
					else
					{
						audioClip = errorClip;
					}
				}
				else if (otherTag.Equals("Tap") && !other.GetComponent<Tap>().IsLocked())
				{
					carriedItem = other.GetComponent<Tap>().RemoveBeverage();
				}

				if (carriedItem != null)
				{
					audioClip = carriedItem.GetAudioClip();
				}
			}

			PlayAudioClip();
			UpdateGameObject();
		}
	}
}
