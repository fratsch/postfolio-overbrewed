﻿using DG.Tweening;
using UnityEngine;

namespace Scripts.VFX
{
	public class SlidingDoor : MonoBehaviour
	{
		[SerializeField] private GameObject door;

		private readonly Vector3 openVector = new Vector3(0.0f, 0.0f, 0.0f);

		private readonly Vector3 closeVector = new Vector3(0.0f, 180.0f, 0.0f);

		private Transform doorTransform;

		private void Start()
		{
			doorTransform = door.transform;
		}

		public void Slide(float duration)
		{
			Sequence s = DOTween.Sequence();
			s.Append(doorTransform.DORotate(closeVector, 1.0f));
			s.AppendInterval(duration);
			s.Append(doorTransform.DORotate(openVector, 1.0f));
		}
	}
}
