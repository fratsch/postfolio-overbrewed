﻿using Scripts.Brewing.ItemTypes;
using UnityEngine;

namespace Scripts.Player
{
	[CreateAssetMenu(fileName = "New BazookaAmmunition", menuName = "OverBrewed/Player/BazookaAmmunition")]
	public class BazookaAmmunition : ScriptableObject
	{
		public IngredientType ammoType;
	}
}
