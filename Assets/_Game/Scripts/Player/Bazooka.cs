﻿// written together with Tobias Schmidt

using Scripts.Brewing.ItemTypes;
using Scripts.Player;
using UnityEngine;

public class Bazooka : MonoBehaviour
{
	[SerializeField] private GameObject bazookaObject;
	public BazookaAmmunition ammunition = null;

	private GameObject itemSpawn;

	private void Start()
	{
		ammunition.ammoType = null;
		bazookaObject = bazookaObject.transform.GetChild(0).gameObject;
		foreach (Transform t in bazookaObject.transform)
		{
			if (t.tag.Equals("ItemSpawn"))
			{
				itemSpawn = t.gameObject;
			}
		}
	}

	// Loads a IngredientType into the Bazooka if there is nothing loaded already
	public IngredientType LoadBazooka(IngredientType ammo)
	{
		IngredientType ingredient = this.ammunition.ammoType;

		ammunition.ammoType = ammo;

		UpdateGameObjects();

		return ingredient;
	}

	// Gives back the Prefab that was loaded in the Bazooka
	// If Bazooka has nothing loaded gives Back nothing
	public GameObject ShootBazooka()
	{
		GameObject result = null;

		if (ammunition.ammoType != null)
		{
			result = ammunition.ammoType.GetPrefab();
		}

		ammunition.ammoType = null;

		UpdateGameObjects();

		return result;
	}

	private void UpdateGameObjects()
	{
		foreach (Transform t in itemSpawn.transform)
		{
			Destroy(t.gameObject);
		}

		if (ammunition.ammoType == null) return;

		GameObject newObject = Instantiate(ammunition.ammoType.GetPrefab(), itemSpawn.transform.position,
			itemSpawn.transform.rotation) as GameObject;
		newObject.transform.parent = itemSpawn.transform;
	}
}
